# EXAMEN CONCEPTION LOGICIELLE

Examen de conception logicielle 2021 de Hugo BOUTTES

# Deux parties :

# partie serveur dans /serveur

## Installer les dépendances de /serveur

`pip install -r requis.txt`

## Lancer l'application

`uvicorn code:app --reload`

En cliquant sur ce lien vous pouvez lancer l'application :
[Examen](http://127.0.0.1:8000/)


# partie client dans /client

## Installer les dépendances de /client

`pip install -r requis.txt`

## Lancer l'application

`python code.py`


# PS
N'oubliez pas de vous placer dans vos environnements avant de lancer les codes
