import requests

adresse = "http://127.0.0.1:8000/"

nombre_de_carte = 10


# Création du Deck
lien = str(adresse + "creer-un-deck")
req = requests.get(lien)
deck_id = req.json()["deck_id"]
print("deck_id = "+ deck_id)

#Tirage de carte 
dico_tirage= {"nombre_cartes": nombre_de_carte}
lien = str(adresse + "cartes/")
req = requests.post(url = lien , json = dico_tirage)
rep = req.json()

cartes = rep["cards"]

# Comptage des cartes 
def comptage(cartes):
    reponse = {"HEARTS":0,"SPADES":0,"DIAMONDS":0,"CLUBS":0}

    for L in cartes:
        reponse[L["suit"]] += 1
    return reponse

print("Le détail des couleurs des cartes : " )
print(comptage(cartes))
print("Si toutes les cartes ne sont que d'une seule couleur c'est normal !")
print("le paquet de carte n'a pas été mélangé, ceci arrive dans une future màj !")