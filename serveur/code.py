from fastapi import FastAPI 
from pydantic import BaseModel

import requests

class Tirer(BaseModel):
    nombre_cartes:int


app = FastAPI()
deck_id = ""
deck_bool = False

def CreationDeck():
    global deck_id
    req = requests.get("https://deckofcardsapi.com/api/deck/new/")
    dic = req.json()
    deck_id = dic["deck_id"]

# Initialisation
@app.get("/")
def readRoot():
    return "https://deckofcardsapi.com"

# Récupère un deck de l'api et renvoie son id : exposé en GET sur /creer-un-deck/
@app.get("/creer-un-deck")
def CreerDeck():
    CreationDeck()
    deck_bool = True
    return {"deck_id" : deck_id}


#Tire des cartes du deck (tirer x cartes du deck parmi les restantes) :
#exposé en POST sur /cartes/{nombre_cartes} avec un body : {deck_id:str}
@app.post("/cartes")
def TirerCarte(ti:Tirer):
    if not deck_bool:
        CreationDeck()
    url = "https://deckofcardsapi.com/api/deck/"+ str(deck_id) + "/draw/?count=" + str(ti.nombre_cartes)
    req = requests.get(url)
    dic = req.json()
    rep ={}

    if "error" in dic:
        rep = {"deck_id": deck_id, "cards": dic["cards"], "error": dic["error"]}
    else:
        rep = {"deck_id": deck_id,  "cards": dic["cards"]}

    return rep